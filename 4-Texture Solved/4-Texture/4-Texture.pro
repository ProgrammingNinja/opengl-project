#-------------------------------------------------
#
# Project created by QtCreator 2016-08-30T10:46:49
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 4-Test-Code
TEMPLATE = app


SOURCES += main.cpp \
    mainwidget.cpp \
    transform3d.cpp \
    camera3d.cpp \
    input.cpp \
    cube.cpp

HEADERS  += \
    mainwidget.h \
    transform3d.h \
    camera3d.h \
    input.h \
    vertex.h \
    cube.h

FORMS    +=

RESOURCES += \
    textures.qrc \
    shaders.qrc
