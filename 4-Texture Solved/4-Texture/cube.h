#ifndef CUBE_H
#define CUBE_H

#include "transform3d.h"
#include "vertex.h"
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>

class Cube :  public Transform3D, protected QOpenGLFunctions
{
public:
    Cube(QOpenGLShaderProgram *program);
    virtual ~Cube();

    void drawCubeGeometry(QOpenGLShaderProgram *program,  QMatrix4x4 view, QMatrix4x4 projection);

private:
    void initCubeGeometry(QOpenGLShaderProgram *program);

    QOpenGLBuffer m_vertex;
    QOpenGLVertexArrayObject m_object;
};

#endif // CUBE_H
