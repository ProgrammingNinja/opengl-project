#include "cube.h"
#include <iostream>
#include <QVector2D>
#include <QVector3D>

// Front face Verticies
#define VERTEX_FTR Vertex( QVector3D( 0.5f,  0.5f,  0.5f), QVector3D( 1.0f, 0.0f, 0.0f ), QVector2D(0.50f, 0.66f) )
#define VERTEX_FTL Vertex( QVector3D(-0.5f,  0.5f,  0.5f), QVector3D( 0.0f, 1.0f, 0.0f ), QVector2D(0.25f, 0.66f) )
#define VERTEX_FBL Vertex( QVector3D(-0.5f, -0.5f,  0.5f), QVector3D( 0.0f, 0.0f, 1.0f ), QVector2D(0.25f, 0.33f) )
#define VERTEX_FBR Vertex( QVector3D( 0.5f, -0.5f,  0.5f), QVector3D( 0.0f, 0.0f, 0.0f ), QVector2D(0.50f, 0.33f))

// Back face Verticies
#define VERTEX_BTR Vertex( QVector3D( 0.5f,  0.5f, -0.5f), QVector3D( 1.0f, 1.0f, 0.0f ), QVector2D(1.00f, 0.66f) )
#define VERTEX_BTL Vertex( QVector3D(-0.5f,  0.5f, -0.5f), QVector3D( 0.0f, 1.0f, 1.0f ), QVector2D(0.75f, 0.66f) )
#define VERTEX_BBL Vertex( QVector3D(-0.5f, -0.5f, -0.5f), QVector3D( 1.0f, 0.0f, 1.0f ), QVector2D(0.75f, 0.33f) )
#define VERTEX_BBR Vertex( QVector3D( 0.5f, -0.5f, -0.5f), QVector3D( 1.0f, 1.0f, 1.0f ), QVector2D(1.00f, 0.33f) )

// Top face Verticies
#define VERTEX_FTL_T Vertex(QVector3D(-0.5f,  0.5f,  0.5f), QVector3D(1.0f, 1.0f,  1.0f) , QVector2D(0.25f, 0.66f))
#define VERTEX_FTR_T Vertex(QVector3D( 0.5f,  0.5f,  0.5f), QVector3D(1.0f, 1.0f,  1.0f) , QVector2D(0.50f, 0.66f))
#define VERTEX_BTL_T Vertex(QVector3D(-0.5f,  0.5f, -0.5f), QVector3D(1.0f, 1.0f,  1.0f) , QVector2D(0.25f, 1.00f))
#define VERTEX_BTR_T Vertex(QVector3D( 0.5f,  0.5f, -0.5f), QVector3D(1.0f, 1.0f,  1.0f) , QVector2D(0.50f, 1.00f))

// Bottom face Verticies
#define VERTEX_BBL_B Vertex(QVector3D(-0.5f, -0.5f, -0.5f), QVector3D(1.0f, 1.0f,  1.0f) , QVector2D(0.25f, 0.00f))
#define VERTEX_BBR_B Vertex(QVector3D( 0.5f, -0.5f, -0.5f), QVector3D(1.0f, 1.0f,  1.0f) , QVector2D(0.50f, 0.00f))
#define VERTEX_FBL_B Vertex(QVector3D(-0.5f, -0.5f,  0.5f), QVector3D(1.0f, 1.0f,  1.0f) , QVector2D(0.25f, 0.33f))
#define VERTEX_FBR_B Vertex(QVector3D( 0.5f, -0.5f,  0.5f), QVector3D(1.0f, 1.0f,  1.0f) , QVector2D(0.50f, 0.33f))

// Right face Verticies
#define VERTEX_FBR_R Vertex(QVector3D( 0.5f, -0.5f,  0.5f), QVector3D(1.0f, 1.0f,  1.0f) , QVector2D(0.50f, 0.33f)) // v4
#define VERTEX_BBR_R Vertex(QVector3D( 0.5f, -0.5f, -0.5f), QVector3D(1.0f, 1.0f,  1.0f) , QVector2D(0.75f, 0.33f)) // v5
#define VERTEX_FTR_R Vertex(QVector3D( 0.5f,  0.5f,  0.5f), QVector3D(1.0f, 1.0f,  1.0f) , QVector2D(0.50f, 0.66f))  // v6
#define VERTEX_BTR_R Vertex(QVector3D( 0.5f,  0.5f, -0.5f), QVector3D(1.0f, 1.0f,  1.0f) , QVector2D(0.75f, 0.66f)) // v7

// Left face Verticies
#define VERTEX_BBL_L Vertex(QVector3D(-0.5f, -0.5f, -0.5f), QVector3D(1.0f, 1.0f,  1.0f) , QVector2D(0.00f, 0.33f)) // v12
#define VERTEX_FBL_L Vertex(QVector3D(-0.5f, -0.5f,  0.5f), QVector3D(1.0f, 1.0f,  1.0f) , QVector2D(0.25f, 0.33f))  // v13
#define VERTEX_BTL_L Vertex(QVector3D(-0.5f,  0.5f, -0.5f), QVector3D(1.0f, 1.0f,  1.0f) , QVector2D(0.00f, 0.66f)) // v14
#define VERTEX_FTL_L Vertex(QVector3D(-0.5f,  0.5f,  0.5f), QVector3D(1.0f, 1.0f,  1.0f) , QVector2D(0.25f, 0.66f))  // v15



// Create a colored cube
static const Vertex sg_vertexes[] = {
  // Face 1 (Front)
    VERTEX_FTR, VERTEX_FTL, VERTEX_FBL,
    VERTEX_FBL, VERTEX_FBR, VERTEX_FTR,
  // Face 2 (Back)
    VERTEX_BBR, VERTEX_BTL, VERTEX_BTR,
    VERTEX_BTL, VERTEX_BBR, VERTEX_BBL,
  // Face 3 (Top)
    VERTEX_FTR_T, VERTEX_BTR_T, VERTEX_BTL_T,
    VERTEX_BTL_T, VERTEX_FTL_T, VERTEX_FTR_T,
  // Face 4 (Bottom)
    VERTEX_FBR_B, VERTEX_FBL_B, VERTEX_BBL_B,
    VERTEX_BBL_B, VERTEX_BBR_B, VERTEX_FBR_B,
  // Face 5 (Left)
    VERTEX_FBL_L, VERTEX_FTL_L, VERTEX_BTL_L,
    VERTEX_FBL_L, VERTEX_BTL_L, VERTEX_BBL_L,
  // Face 6 (Right)
    VERTEX_FTR_R, VERTEX_FBR_R, VERTEX_BBR_R,
    VERTEX_BBR_R, VERTEX_BTR_R, VERTEX_FTR_R
};

#undef VERTEX_BBR
#undef VERTEX_BBL
#undef VERTEX_BTL
#undef VERTEX_BTR

#undef VERTEX_FBR
#undef VERTEX_FBL
#undef VERTEX_FTL
#undef VERTEX_FTR


Cube::Cube(QOpenGLShaderProgram *program)
{
    initializeOpenGLFunctions();
    m_vertex.create();
    // Initializes cube geometry and transfers it to VBOs
    initCubeGeometry(program);
}

Cube::~Cube()
{
    m_object.destroy();
    m_vertex.destroy(); 
}

void Cube::initCubeGeometry(QOpenGLShaderProgram *program)
{

    m_vertex.bind();
    m_vertex.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_vertex.allocate(sg_vertexes, sizeof(sg_vertexes));

    m_object.create();
    m_object.bind();

    // Tell OpenGL programmable pipeline how to locate vertex position data
    int vertexLocation = program->attributeLocation("a_position");
    program->enableAttributeArray(vertexLocation);
    program->setAttributeBuffer(vertexLocation, GL_FLOAT, Vertex::positionOffset(), Vertex::PositionTupleSize, Vertex::stride());

    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    int texcoordLocation = program->attributeLocation("a_texcoord");
    program->enableAttributeArray(texcoordLocation);
    program->setAttributeBuffer(texcoordLocation, GL_FLOAT, Vertex::uvOffset(), Vertex::UVTupleSize, Vertex::stride());


    m_object.release();
    m_vertex.release();
}

void Cube::drawCubeGeometry(QOpenGLShaderProgram *program, QMatrix4x4 view, QMatrix4x4 projection)
{
    m_object.bind();

    // Set modelview-projection matrix
    QMatrix4x4 mvp = projection * view * toMatrix();
    program->setUniformValue("mvp_matrix", mvp );
    glDrawArrays(GL_TRIANGLES, 0, sizeof(sg_vertexes) / sizeof(sg_vertexes[0]));

    m_object.release();
}
